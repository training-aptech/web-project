# Web Project - APTECH

## Information
- Class: C1709L
- Course: Project web development

## Software for dev

1. __Source management:__ Git [link download](https://git-scm.com/)
2. __Code Editor:__ Visual Code [link download](https://code.visualstudio.com/)
3. __Extendtion for Visual Code:__ [Market Place](https://marketplace.visualstudio.com/)
    1. Auto Close Tag
    2. Auto Rename Tag
    3. Beautify
    4. Path Intellisence
    5. PHP IntelliSense
    6. ESLint
    7. Bootstrap 4, Font awesome 4, Font Awesome 5 Free & Pro snippets
    8. Markdown All in One
    9. Material Icon Theme
    10. Atom One Dark Theme
    11. Bracket Pair Colorizer
4. __Web Server:__
    1. XAMPP [link download](https://www.apachefriends.org/index.html)
    2. WAMP [link download](http://www.wampserver.com/en/)
    3. MacOS - MAMP [link download](https://www.mamp.info/en/)
5. __Design database tools:__ MySQL Workbench [link down](https://www.mysql.com/products/workbench/)
    - For Windows: 
        - Microsoft .NET Framework 4.5 [link download](https://www.microsoft.com/en-us/download/details.aspx?id=30653)
        - Visual C++ Redistributable for Visual Studio 2015 [link download](https://www.microsoft.com/en-us/download/details.aspx?id=48145)
 
## Project information

1. __Repository:__ https://bitbucket.org/training-aptech/web-project/src/master/

- Step 01: Clone project

```
$ git clone https://bitbucket.org/training-aptech/web-project/src/master/
```
    
- Step 02: Update source

```
$ git pull origin master
```

- Step 03: Commit and Update code to Server repository

```
$ git status
$ git add .
$ git status
$ git commit -am "..."
$ git push origin master
```